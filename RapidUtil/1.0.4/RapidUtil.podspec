Pod::Spec.new do |s|
  s.name             = "RapidUtil"
  s.version          = "1.0.4"
  s.summary          = "This is a library that allows the faster development of projects since it contains numerous utilities"
  s.description      = "This is a library that allows the faster development of projects since it contains numerous utilities, these allow us to manage dates, currency format, navigation and http communication among other functionalities. Based on this, an attempt is made to have a collection of the most used functions by developers."
  s.homepage         = "https://gitlab.com/dpolania/rapidutil.git"
  s.license          = 'MIT'
  s.author           = { 'David Camilo Polania Losada' => 'dpolania@outlook.es' }
  s.source           = { :git => 'https://gitlab.com/dpolania/rapidutil.git', :tag => s.version.to_s }
  s.platform     = :ios, '9.0'
  s.requires_arc = true
  s.source_files = 'RapidUtil/Classes/**/*'
  s.swift_version = '4.1'
end
